import os

from conans.errors import ConanInvalidConfiguration
from conans import ConanFile, CMake, tools

required_conan_version = ">=1.43.0"

class DawUtfRangeConan(ConanFile):
    name = "daw_parse_json"
    license = "BSL-1.0"
    url = "https://github.com/conan-io/conan-center-index"
    description = "Range operations on character arrays"
    topics = ("utf", "validator", "iterator")
    homepage = "https://github.com/beached/utf_range/"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "cmake_find_package",

    _compiler_required_cpp17 = {
        "Visual Studio": "16",
        "gcc": "8",
        "clang": "7",
        "apple-clang": "12.0",
    }

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def validate(self):
        if self.settings.get_safe("compiler.cppstd"):
            tools.check_min_cppstd(self, "17")

        minimum_version = self._compiler_required_cpp17.get(str(self.settings.compiler), False)
        if minimum_version:
            if tools.Version(self.settings.compiler.version) < minimum_version:
                raise ConanInvalidConfiguration("{} requires C++17, which your compiler does not support.".format(self.name))
        else:
            self.output.warn("{} requires C++17. Your compiler is unknown. Assuming it supports C++17.".format(self.name))

    def requirements(self):
        self.requires("daw_header_libraries/1.29.7")
        self.requires("daw_utf_range/2.2.0")
        self.requires("date/3.0.1")
        self.requires("boost/1.78.0")
        self.requires("libcurl/7.80.0")
        self.requires("openssl/1.1.1n")

    def source(self):
        tools.get(**self.conan_data["sources"][self.version], destination=self._source_subfolder, strip_root=True)

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["DAW_USE_PACKAGE_MANAGEMENT"] = True
        # cmake.definitions["CMAKE_CXX_FLAGS"] = "-DDAW_STRINGVIEW_VERSION=1"
        cmake.configure(source_folder=self._source_subfolder)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy("LICENSE*", "licenses", self._source_subfolder)
        cmake = self._configure_cmake()
        cmake.install()
        tools.rmdir(os.path.join(self.package_folder, "lib", "cmake"))
        tools.rmdir(os.path.join(self.package_folder, "share"))

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "parse_json")
        self.cpp_info.set_property("cmake_target_name", "daw::parse_json")

        self.cpp_info.filenames["cmake_find_package"] = "parse_json"
        self.cpp_info.filenames["cmake_find_package_multi"] = "parse_json"
        self.cpp_info.names["cmake_find_package"] = "daw"
        self.cpp_info.names["cmake_find_package_multi"] = "daw"

        self.cpp_info.components["parse_json"].names["cmake_find_package"] = "parse_json"
        self.cpp_info.components["parse_json"].names["cmake_find_package_multi"] = "parse_json"
        self.cpp_info.components["parse_json"].set_property("cmake_target_name", "daw::parse_json")
        self.cpp_info.components["parse_json"].libs = ["parse_json"]
        self.cpp_info.components["parse_json"].requires = [
            "daw_header_libraries::daw",
            "daw_utf_range::daw",
            "date::date",
            "boost::system",
            "boost::filesystem",
            "boost::regex",
            "boost::iostreams",
            "libcurl::libcurl",
            "openssl::SSL"
            "openssl::Crypto",
        ]

        self.cpp_info.components["parse_json_v2"].names["cmake_find_package"] = "parse_json_v2"
        self.cpp_info.components["parse_json_v2"].names["cmake_find_package_multi"] = "parse_json_v2"
        self.cpp_info.components["parse_json_v2"].set_property("cmake_target_name", "daw::parse_json_v2")
        self.cpp_info.components["parse_json_v2"].libs = ["parse_json_v2"]
        self.cpp_info.components["parse_json_v2"].requires = [
            "daw_header_libraries::daw",
            "daw_utf_range::daw",
            "date::date",
            "boost::boost",
            "boost::system",
            "boost::filesystem",
            "boost::regex",
            "boost::iostreams",
            "libcurl::libcurl",
            "openssl::SSL"
            "openssl::Crypto",
            "parse_json",
        ]
