#include "daw/json/daw_json_parser.h"
#include <iostream>
#include <string>

int main() {
    std::string sample(u8R"(
{
    "key1": "value1",
    "key2": 5,
    "key3": 5.343 
}    
    )");

    auto obj = daw::json::parse_json(sample);

    std::cout << std::boolalpha << obj.is_object() << '\n';

    return 0;
}

